# Пример запуска selenium-теста

Запускает `selenium-webdriver` в Chrome для дефолтного Rails-приложения. Пример работает только для Google Chrome, настройка других браузеров не рассматривается.

Для запуска нужен Chrome **версии 77 и выше**.

## Перед запуском

Необходимо установить [Selenium ChromeDriver](https://github.com/SeleniumHQ/selenium/wiki/ChromeDriver).

## Запуск примера

`bundle install`

`rails s`

в другом терминале

`ruby selenium.rb`

Код можно посмотреть в файле `selenium.rb`, все остальное - по умолчанию.
